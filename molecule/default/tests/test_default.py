import pytest
import os
import testinfra.utils.ansible_runner
import time

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture(scope="module", autouse=True)
def setup(host):
    for i in range(200):
        cmd = host.run('curl --insecure --fail --silent --output /dev/null https://ics-ans-role-artifactory-default/ui/')
        if cmd.rc == 0:
            return
        time.sleep(3)
    raise RuntimeError('Timed out waiting for application to start.')


def test_artifactory_containers(host):
    with host.sudo():
        for container in host.docker.get_containers(name=["artifactory", "artifactory_database", "traefik_proxy"]):
            assert container.is_running


def test_artifactory_api(host):
    cmd = host.run("curl --insecure --fail https://ics-ans-role-artifactory-default/router/api/v1/system/ping")
    assert cmd.rc == 0
    assert cmd.stdout == "OK"


def test_artifactory_webapp(host):
    cmd = host.run("curl --insecure --fail https://ics-ans-role-artifactory-default/ui/")
    assert cmd.rc == 0
    assert '<title>JFrog</title>' in cmd.stdout
